from flask import Flask, request, jsonify
import mysql.connector
from functools import wraps

app = Flask(__name__)

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="",
    database="utsapi"
)        

# print("Status Database : ", mydb.is_connected())
# exit()

@app.route("/camera", methods=['GET'])
def getcamera():
    try:
        cursor = mydb.cursor()
        cursor.execute("SELECT * FROM data_camera")
        result = []
        for i in cursor.fetchall():
            result.append({
                "id_camera": i[0],
                "name_camera": i[1],
                "type_camera": i[2]
            })
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/camera/<int:id>", methods=['GET'])
def getcamerabyid(id):
    try:
        cursor = mydb.cursor()
        cursor.execute("SELECT * FROM data_camera WHERE id_camera = %s" % (id,))
        data = cursor.fetchone()
        result = {
                    "id_camera": data[0],
                    "name_camera": data[1],
                    "type_camera": data[2]
                }
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/camera", methods=['POST'])
def insertcamera():
    try:
        data = request.json
        cursor = mydb.cursor()
        sql = "INSERT INTO data_camera (id_camera, name_camera, type_camera) VALUES (NULL, %s, %s)"
        value = (data['name_camera'], data['type_camera'])
        cursor.execute(sql, value)
        mydb.commit()
        return jsonify({"data":"1 Record Inserted!", "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/camera/<int:id>", methods=['PUT'])
def updatecamerabyid(id):
    try:
        data = request.json
        cursor = mydb.cursor()
        sql = "UPDATE data_camera SET name_camera = %s, type_camera = %s WHERE id_camera = %s"
        value = (data['name_camera'], data['type_camera'], id)
        cursor.execute(sql, value)
        mydb.commit()
        return jsonify({"data":"1 Record Affected!", "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/camera/<int:id>", methods=['DELETE'])
def deletecamerabyid(id):
    try:
        cursor = mydb.cursor()
        cursor.execute("DELETE FROM data_camera WHERE id_camera = %s" % (id,))
        return jsonify({"data":"1 Record Deleted!", "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/penjualan", methods=['GET'])
def getpenjualan():
    try:
        cursor = mydb.cursor()
        cursor.execute("SELECT * FROM data_penjualan INNER JOIN data_camera ON data_penjualan.id_camera=data_camera.id_camera;")
        result = []
        for i in cursor.fetchall():
            # result.append(i)
            result.append({
                "id_penjualan": i[0],
                "id_camera": i[1],
                "harga": i[2],
                "jumlah": i[3],
                "name_camera": i[6],
                "type_camera": i[7]
            })
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/penjualan/<int:id>", methods=['GET'])
def getpenjualanbyid(id):
    try:
        cursor = mydb.cursor()
        cursor.execute("SELECT * FROM data_penjualan INNER JOIN data_camera ON data_penjualan.id_camera=data_camera.id_camera WHERE data_penjualan.id_penjualan = %s" % (id,))
        i = cursor.fetchone()
        result = {
                "id_penjualan": i[0],
                "id_camera": i[1],
                "harga": i[2],
                "jumlah": i[3],
                "name_camera": i[6],
                "type_camera": i[7]
            }
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/penjualan", methods=['POST'])
def insertpenjualan():
    try:
        data = request.json
        cursor = mydb.cursor()
        sql = "INSERT INTO data_penjualan (id_penjualan, id_camera, harga, jumlah) VALUES (NULL, %s, %s, %s)"
        value = (data['id_camera'],data['harga'],data['jumlah'])
        cursor.execute(sql, value)
        mydb.commit()
        return jsonify({"data":"1 Record Inserted!", "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/penjualan/<int:id>", methods=['PUT'])
def updatepenjualanbyid(id):
    try:
        data = request.json
        cursor = mydb.cursor()
        sql = "UPDATE data_penjualan SET id_camera = %s, harga = %s, jumlah = %s WHERE id_penjualan = %s"
        value = (data['id_camera'], data['harga'], data['jumlah'], id)
        cursor.execute(sql, value)
        mydb.commit()
        return jsonify({"data":"1 Record Affected!", "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/penjualan/<int:id>", methods=['DELETE'])
def deletepenjualanbyid(id):
    try:
        cursor = mydb.cursor()
        cursor.execute("DELETE FROM data_penjualan WHERE id_penjualan = %s" % (id,))
        return jsonify({"data":"1 Record Deleted!", "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

if "__main__" == __name__:
    # app.run(debug=True, host="0.0.0.0", port=5000)
    app.run(debug=True, port=1000)